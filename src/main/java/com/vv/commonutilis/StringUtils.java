/**
 * 
 */
package com.vv.commonutilis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vv.commonutilis.services.CommonConstants;

/**
 * @author Kathiravan
 * @Created Date : Wed Sep 23 19:33:18 IST 2020
 */
public class StringUtils implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Request List of String Values Responce is List of Long Values
	public static List<Long> convertListOfStringToLong(List<String> listOfString) {
		List<Long> responceList = new ArrayList<Long>();
		try {
			listOfString.forEach((string -> {
				responceList.add(NumericUtils.convertStringToLong(string));
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responceList;
	}

	// Request List of Long Values Responce is List of String Values
	public static List<String> convertListOfLongToString(List<Long> listOfLong) {
		List<String> responceList = new ArrayList<String>();
		try {
			listOfLong.forEach((string -> {
				responceList.add(NumericUtils.convertLongToString(string));
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responceList;
	}

	public static String emptyOrNullWithHypen(String request) {
		try {
			request = (org.apache.commons.lang3.StringUtils.isNotBlank(request) && !"null".equalsIgnoreCase(request))
					? request
					: "-";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return request;
	}

	public static String checkNull(String request) {
		try {
			request = (org.apache.commons.lang3.StringUtils.isNotBlank(request) && !"null".equalsIgnoreCase(request))
					? request
					: CommonConstants.EMPTY_STRING;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return request;
	}

	public static String convertTheListOfStringToString(List<String> request) {
		String res = "";
		try {
			if (request != null && request.size() > 0) {
				res = String.join(", ", request);
				return res;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static List<String> convertStringToListOfString(String request) {
		List<String> array = new ArrayList<String>();
		try {
			if (org.apache.commons.lang3.StringUtils.isNotBlank(request)) {
				array = Arrays.asList(request.split(" , "));
				return array;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
}

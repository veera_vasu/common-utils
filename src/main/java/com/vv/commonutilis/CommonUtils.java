/**
 * 
 */
package com.vv.commonutilis;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
//import java.util.Comparator;
import java.util.Date;
//import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
//import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpMethod;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vv.commonutilis.dto.CommonReponseDto;
import com.vv.commonutilis.dto.CommonRequestDto;
import com.vv.commonutilis.services.CommonConstants;

/**
 * @author Veeravasu
 * 
 */
public class CommonUtils implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static ObjectMapper mapper = new ObjectMapper();

	public static String ifCheckStringNull(String request) {
		try {
			request = request != null ? request : "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return request;
	}

	public static String getStackTrace(final Throwable throwable) {
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		return sw.getBuffer().toString();
	}

	public static boolean getCode(List<String> list, String code) {
		String result = (list != null && list.size() > 0)
				? list.stream().filter(inCode -> code.equals(inCode)).findAny().orElse(null)
				: null;
		if (StringUtils.isNotBlank(result)) {
			return false;
		}
		return true;
	}

	public static List<String> list(List<String> list) {
		return list != null ? list : new ArrayList<String>();
	}

	public static void exception(CommonReponseDto<?> commonReponseDto, Exception e, List<String> errors) {
		commonReponseDto.setStatus(CommonConstants.ERROR);
		commonReponseDto.setResponseStatus(CommonConstants.ERROR);
		commonReponseDto.setMessage(e.getMessage());
		commonReponseDto.setResponseMessage(e.getMessage());
		commonReponseDto.setError(getStackTrace(e));
		errors.add(getStackTrace(e));
		commonReponseDto.setErrorDescriptions(errors);
	}

	public static void exception(CommonReponseDto<?> commonReponseDto, Exception e) {
		commonReponseDto.setStatus(CommonConstants.ERROR);
		commonReponseDto.setResponseStatus(CommonConstants.ERROR);
		commonReponseDto.setMessage(e.getMessage());
		commonReponseDto.setResponseMessage(e.getMessage());
		commonReponseDto.setError(getStackTrace(e));
	}

	public static void actionNotAllowed(CommonReponseDto<?> commonReponseDto) {
		commonReponseDto.setStatus(CommonConstants.ERROR);
		commonReponseDto.setMessage(CommonConstants.ACTIONNOTALLOWED);
		commonReponseDto.setError(CommonConstants.ACTIONNOTALLOWED);
	}

//	public static String getFullName(UserDto userDto) {
//		String fullName = "-";
//		if (userDto != null) {
//			String lastName = StringUtils.isNotBlank(userDto.getLastName()) ? " " + userDto.getLastName()
//					: CommonConstants.EMPTY_STRING;
//			fullName = userDto.getFirstName() + lastName;
//		}
//		return fullName;
//	}

	public static List<String> errorDescription(List<String> errors, String error) {
		if (errors != null) {
			errors.add(error);
		} else {
			errors = new ArrayList<String>();
			errors.add(error);
		}
		return errors;
	}

	// Get the current line number in Java
	public static int getLineNumber() {
		return new Throwable().getStackTrace()[0].getLineNumber();
	}

	public static String getLogMessage(int lineNo, String className, String methodName) {
		return lineNo + "~" + className + "~" + methodName + "~ ";
	}

	public static String getLogMessage(Exception e) {
		if (e != null) {
			StackTraceElement[] stackTrace = e.getStackTrace();
			String className = stackTrace[0].getClassName();
			String methodName = stackTrace[0].getMethodName();
			int lineNo = stackTrace[0].getLineNumber();
			return lineNo + "~" + className + "~" + methodName + "~ ";
		}
		return CommonConstants.EMPTY_STRING;
	}

	public static String getClassName(Class<?> myClass) {
		return myClass.getName();
	}

	public static Properties applicationProperties(String jsonString) {
		JsonRequest<Properties> jsonRequest = new JsonRequest<Properties>();
		Properties appProps = null;
		try {
			appProps = jsonRequest.convertJsonStringToObject(jsonString, Properties.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return appProps;
	}

	public static Properties applicationProperties(String jsonString, String key) {
		JsonRequest<Properties> jsonRequest = new JsonRequest<Properties>();
		Properties appProps = new Properties();
		try {
			Map<String, LinkedHashMap<String, String>> mapProps = convertJsonStringToObjectMap(jsonString);

			if (StringUtils.isNotBlank(key)) {
				for (Entry<String, LinkedHashMap<String, String>> map : mapProps.entrySet()) {
					if (key.equalsIgnoreCase(map.getKey())) {
						LinkedHashMap<String, String> val = map.getValue();
						System.out.println(val);
						appProps.putAll(val);
						return appProps;
						// appProps = jsonRequest.convertJsonStringToObject(mapString,
						// Properties.class);
					}
				}
			} else {
				appProps = jsonRequest.convertJsonStringToObject(jsonString, Properties.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return appProps;
	}

	public static LinkedHashMap<String, String> applicationPropertiesAsMap(String jsonString, String key) {
		try {
			Map<String, LinkedHashMap<String, String>> mapProps = convertJsonStringToObjectMap(jsonString);

			if (StringUtils.isNotBlank(key)) {
				for (Entry<String, LinkedHashMap<String, String>> map : mapProps.entrySet()) {
					if (key.equalsIgnoreCase(map.getKey())) {
						LinkedHashMap<String, String> value = map.getValue();
						return value;
						// appProps = jsonRequest.convertJsonStringToObject(mapString,
						// Properties.class);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Properties applicationProperties(String jsonString, List<String> keys) {
		JsonRequest<Properties> jsonRequest = new JsonRequest<Properties>();
		Properties appProps = new Properties();
		try {
			Map<String, LinkedHashMap<String, String>> mapProps = convertJsonStringToObjectMap(jsonString);

			if (keys != null && keys.size() > 0) {
				Map<String, String> map = new LinkedHashMap<String, String>();
				for (String key : keys) {
					LinkedHashMap<String, String> mapString = mapProps.get(key);
				}
				appProps.putAll(map);
				return appProps;
			} else {
				appProps = jsonRequest.convertJsonStringToObject(jsonString, Properties.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return appProps;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, LinkedHashMap<String, String>> convertJsonStringToObjectMap(String json)
			throws JsonParseException, JsonMappingException, IOException {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.readValue(json, Map.class);
	}

	@SuppressWarnings("unchecked")
	public static Map<String, String> convertJsonObjectToMap(Object jsonOb)
			throws JsonParseException, JsonMappingException, IOException {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.readValue(String.valueOf(jsonOb), Map.class);
	}

//	public static String fetchApi(String apiCode, String apiUrl) {
//		RestTemplate restTemplate = new RestTemplate();
//		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(apiUrl);
//		url.queryParam("code", apiCode);
//		ApiConfigDto responseDto = restTemplate
//				.exchange(url.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ApiConfigDto>() {
//				}).getBody();
//		return responseDto != null
//				? (com.fa.comp.util.StringUtils.checkNull(responseDto.getUrl())
//						+ com.fa.comp.util.StringUtils.checkNull(responseDto.getEndpoint()))
//				: CommonConstants.EMPTY_STRING;
//	}

	public static Boolean trueFalse(String value) {
		if (StringUtils.isNotBlank(value)) {
			switch (value) {
			case CommonConstants.Y:
			case CommonConstants.YES:
			case CommonConstants.Yes:
				return CommonConstants.TRUE;
			case CommonConstants.N:
			case CommonConstants.NO:
			case CommonConstants.No:
				return CommonConstants.FALSE;
			default:
				break;
			}
		}
		return CommonConstants.FALSE;
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> (t != null ? seen.add(keyExtractor.apply(t)) : null);
	}

//	public static List<Long> getStatusIds(List<StatusRoleMappingDto> list) {
//		if (list != null && list.size() > 0) {
//			List<Long> ids = list.stream().map(StatusRoleMappingDto::getStatusId).collect(Collectors.toList());
//			return ids;
//		}
//		return null;
//	}

	// Fetch the STATUS based on statusId-Status Ex: 7-Pending for non-potential by
	// qc2..
	public static String getDisplayName(String value) {
		if (StringUtils.isNotBlank(value)) {
			String[] split = value.split("-");
			StringBuffer sb = new StringBuffer();
			if (split.length > 1) {
				for (int i = 1; i <= split.length - 1; i++) {
					if (i == 1) {
						sb.append(split[i]);
					} else {
						sb.append("-" + split[i]);
					}
				}
			}
			String status = sb.toString();
			return status;
		}
		return null;
	}

	// Decodes a URL encoded string using `UTF-8`
	public static String decodeValue(String value) {
		try {
			if (StringUtils.isNotBlank(value)) {
				return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new RuntimeException(ex.getCause());
		}
		return null;
	}

	public static List<Long> stringToLongList(Set<String> keys) {
		return keys.stream().map(s -> NumericUtils.convertStringToLong(s)).collect(Collectors.toList());
	}

	public static void main(String[] args) {
		System.out.println(Integer.MAX_VALUE);
	}

	public static List<String> getArray(String value, String delimit) {
		if (StringUtils.isNotBlank(value)) {
			String[] split = value.split(delimit);
			List<String> asList = Arrays.asList(split);
			return asList;
		}
		return null;
	}

//	public static Set<String> getDistinctInvTaskId(List<?> list) {
//		if (list != null && list.size() > 0) {
//			Set<String> invList = list.stream().map(InvestigationDetailsDto::getTaskId).collect(Collectors.toSet());
//			return invList;
//		}
//		return null;
//	}

	public static String getBase64String(String plainString) {
		String encodeString = null;
		try {
			encodeString = new String(java.util.Base64.getEncoder().encodeToString(plainString.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
			encodeString = null;
		}
		return encodeString;
	}

	public static boolean isPresentArray(List<String> list, String code) {
		String result = (list != null && list.size() > 0 && StringUtils.isNotBlank(code))
				? list.stream().filter(inCode -> code.equals(inCode)).findFirst().orElse(null)
				: null;
		if (StringUtils.isNotBlank(result)) {
			return true;
		}
		return false;
	}

	public static boolean isPresentInString(String value, String delimit, String code) {
		if (StringUtils.isNotBlank(value)) {
			String[] split = value.split(delimit);
			List<String> list = Arrays.asList(split);
			String result = (list != null && list.size() > 0)
					? list.stream().filter(inCode -> code.equals(inCode)).findFirst().orElse(null)
					: null;
			if (StringUtils.isNotBlank(result)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isNullBool(Boolean val) {
		if (val == null) {
			return CommonConstants.FALSE;
		}
		return val;
	}

	public static boolean isTrue(Boolean val) {
		if (val != null && val) {
			return CommonConstants.TRUE;
		}
		return CommonConstants.FALSE;
	}

	public static String getSimpleClassName(Class<?> myClass) {
		return myClass.getSimpleName();
	}

	public static List<Long> validateList(List<Long> list) {
		if (list != null && list.size() > 0) {
			return list;
		}
		return Collections.emptyList();
	}

	public static Set<Long> validateSet(Set<Long> list) {
		if (list != null && list.size() > 0) {
			return list;
		}
		return Collections.emptySet();
	}

	public static String getStatusName(String value) {
		if (StringUtils.isNotBlank(value)) {
			String[] split = value.split("-");
			String status = split[1];
			return status;
		}
		return null;
	}

	public static boolean compareRoleCodes(List<String> list, String code) {
		String result = (list != null && list.size() > 0)
				? list.stream().filter(inCode -> code.equals(inCode)).findAny().orElse(null)
				: null;
		if (StringUtils.isNotBlank(result)) {
			return true;
		}
		return false;
	}

	public static Boolean checkNullAndEquals(String passingValue, String comparingValue) {
		Boolean boolean1 = CommonConstants.FALSE;
		try {

			if (StringUtils.isNotBlank(passingValue) && passingValue.equalsIgnoreCase(comparingValue)) {
				boolean1 = CommonConstants.TRUE;
			} else {
				boolean1 = CommonConstants.FALSE;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return boolean1;
	}

	public static String convertDateToStringYYYYMMDDhhmmss(Date val) {
		String ouput = "";
		try {
			if (null != val) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				ouput = dateFormat.format(val);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ouput;
	}

	/*
	 * To convert CommonRequestDto to String
	 * 
	 * @param CommonRequestDto
	 * 
	 * @return jsonString
	 * 
	 * @author yukesh
	 */

	public static String convertcoommonRequestDToToString(CommonRequestDto commonRequestDto) {
		String jsonString = null;
		try {

			ObjectMapper mapper = new ObjectMapper();
			jsonString = mapper.writeValueAsString(commonRequestDto);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonString;
	}

	/*
	 * To convert string to CommonRequestDto
	 * 
	 * @param commonRequestDtoString
	 * 
	 * @return CommonRequestDto
	 * 
	 * @author yukesh
	 */

	public CommonRequestDto convertStringToCoommonRequestDTo(String commonRequestDtoString) {
		CommonRequestDto commonRequestDto = null;
		try {
			commonRequestDto = mapper.readValue(commonRequestDtoString, CommonRequestDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return commonRequestDto;
	}

	public static String checkNull(String text) {
		if (null != text && !CommonConstants.CHECK_STRING_NULL.equalsIgnoreCase(text) && !text.isEmpty()) {
			return text;
		} else {
			return "";
		}
	}

//	public static ApiConfigDto getApiConfigDto(String apiCode, String apiUrl) {
//		ApiConfigDto apiConfigDto = null;
//		try {
//			RestTemplate restTemplate = new RestTemplate();
//			UriComponentsBuilder url = UriComponentsBuilder.fromUriString(apiUrl);
//			url.queryParam("code", apiCode);
//			apiConfigDto = restTemplate.exchange(url.build().toUriString(), HttpMethod.GET, null,
//					new ParameterizedTypeReference<ApiConfigDto>() {
//					}).getBody();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return apiConfigDto;
//	}

	// encrypt as a string

//	public static <T> String getWorkFlowRequestDtoFromReq(T t) throws Exception {
//		String workFlowRequestString = null;
//		try {
//			if (t instanceof InvestigationTaskDto) {
//				workFlowRequestString = getObjectStringUsingBase64(
//						mapper.writeValueAsString((InvestigationTaskDto) t).getBytes());
//			} else if (t instanceof WorkFlowActionTransitionsDto) {
//				workFlowRequestString = getObjectStringUsingBase64(
//						mapper.writeValueAsString((WorkFlowActionTransitionsDto) t).getBytes());
//			} else if (t instanceof InvActionDto) {
//				workFlowRequestString = getObjectStringUsingBase64(
//						mapper.writeValueAsString((InvActionDto) t).getBytes());
//			}
//
//		} catch (Exception e) {
//			throw new Exception(e.getMessage());
//		}
//		return workFlowRequestString;
//	}

//	public static InvestigationTaskDto geInvestigationTaskDtoFromReq(String request) throws Exception {
//		InvestigationTaskDto investigationTaskDto = null;
//		try {
//			investigationTaskDto = mapper.readValue(request, InvestigationTaskDto.class);
//		} catch (Exception e) {
//			throw new Exception(e.getMessage());
//		}
//		return investigationTaskDto;
//	}

	public static String getObjectStringUsingBase64(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}

	public static String getStringToObjectUsingBase64(String base64Str) {
		return new String(Base64.getDecoder().decode(base64Str), StandardCharsets.UTF_8);
	}

	public static List<String> stringSplitFn(String inputStr) {
		List<String> resString = new ArrayList<String>();
		String[] split = inputStr.split("-");
		for (int i = 0; i < split.length; i++) {
			resString.add(split[i]);
		}
		return resString;

	}
}

/**
 * 
 */
package com.vv.commonutilis.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Fasoftwares
 *
 */
@JsonIgnoreProperties
public class CommonSearchDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private String firstName;
	private String lastName;
	private Boolean isDataAvailable;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Boolean getIsDataAvailable() {
		return isDataAvailable;
	}

	public void setIsDataAvailable(Boolean isDataAvailable) {
		this.isDataAvailable = isDataAvailable;
	}

}

/**
 * 
 */
package com.vv.commonutilis.dto;

import java.util.List;

/**
 * @author Veeravasu
 *
 */
public class CommonReponseDto<T> {

	private T responsData;
	private String status;
	private String message;
	private String responseStatus;
	private String responseMessage;
	private String error;
	private List<String> errorDescriptions;
	private List<?> responsDatas;
	private List<?> data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResponsData() {
		return responsData;
	}

	public void setResponsData(T responsData) {
		this.responsData = responsData;
	}

	public List<?> getResponsDatas() {
		return responsDatas;
	}

	public void setResponsDatas(List<?> responsDatas) {
		this.responsDatas = responsDatas;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<String> getErrorDescriptions() {
		return errorDescriptions;
	}

	public void setErrorDescriptions(List<String> errorDescriptions) {
		this.errorDescriptions = errorDescriptions;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

}

/**
 * 
 */
package com.vv.commonutilis.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Veeravasu
 *
 */
public class CommonDataTableDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String status;
	private int draw;
	private long recordsTotal;
	private int recordsFiltered;
	private int totalPages;
	private int lasPage;
	private int currentPage;
	private Integer nextPage;
	private int perPageSize;
	private long from;
	private long to;
	private List<?> data;
	private String sort;
	private String filter;
	private CommonSearchFieldDto searchForm;
	private String start;
	private String length;
	private String message;
	private List<CommonSearchFieldDto> columns;
	private List<?> order;
	private Object search;
	private String responseStatus;
	private String responseMessage;
	private String error;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public long getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(long recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getLasPage() {
		return lasPage;
	}

	public void setLasPage(int lasPage) {
		this.lasPage = lasPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getNextPage() {
		return nextPage;
	}

	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}

	public int getPerPageSize() {
		return perPageSize;
	}

	public void setPerPageSize(int perPageSize) {
		this.perPageSize = perPageSize;
	}

	public long getFrom() {
		return from;
	}

	public void setFrom(long from) {
		this.from = from;
	}

	public long getTo() {
		return to;
	}

	public void setTo(long to) {
		this.to = to;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<CommonSearchFieldDto> getColumns() {
		return columns;
	}

	public void setColumns(List<CommonSearchFieldDto> columns) {
		this.columns = columns;
	}

	public List<?> getOrder() {
		return order;
	}

	public void setOrder(List<?> order) {
		this.order = order;
	}

	public Object getSearch() {
		return search;
	}

	public void setSearch(Object search) {
		this.search = search;
	}

	public CommonSearchFieldDto getSearchForm() {
		return searchForm;
	}

	public void setSearchForm(CommonSearchFieldDto searchForm) {
		this.searchForm = searchForm;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getSort() {
		return sort;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

}

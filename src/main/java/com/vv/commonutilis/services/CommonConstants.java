package com.vv.commonutilis.services;

public interface CommonConstants {

	public String SAVE_MESSAGE = "Data Saved Successfully";
	public Boolean TRUE = Boolean.TRUE;
	public Boolean FALSE = Boolean.FALSE;
	public String SUCCESS = "SUCCESS";
	public String FAILURE = "FAILURE";
	public String ERROR = "ERROR";
	public String NO_USER_FOUND = "No User Found For this UserID";
	public String NO_DATA_FOUND = "No Records Found";
	public String ACTIONNOTALLOWED = "Action Not Allowed";
	public String EMPTY_STRING = "";
	public String ACTIVE = "ACTIVE";
	public String INACTIVE = "INACTIVE";
	public String CHECK_STRING_NULL = "NULL";
	public String YES = "YES";
	public String Yes = "Yes";
	public String Y = "Y";
	public String NO = "NO";
	public String No = "No";
	public String N = "N";
	public String UNDEFINED = "undefined";
	public String ASC = "ASC";
	public String DESC = "DESC";
	
}

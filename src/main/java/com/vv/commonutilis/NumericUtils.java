/**
 * 
 */
package com.vv.commonutilis;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Veeravasu
 * 
 */
public class NumericUtils implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected static final Logger logger = LoggerFactory.getLogger(NumericUtils.class);

	public static Integer convertStringToInteger(String value) {
		try {
			return value == null ? null : Integer.parseInt(value);
		} catch (NumberFormatException e) {
			logger.error("NumericUtils-------convertInteger----Started ");
			return null;
		}
	}

	public static Integer stringToInteger(String value) {
		try {
			return value == null ? 0 : Integer.parseInt(value);
		} catch (NumberFormatException e) {
			logger.error("NumericUtils-------convertInteger----Started ");
			return null;
			// throw new ApplicationException("");
		}
	}

	public static Long convertStringToLong(String value) {
		try {
			return value == null ? null : Long.valueOf(value);
		} catch (NumberFormatException e) {
			logger.error("NumericUtils-------convertInteger----Started ");
			return null;
			// throw new ApplicationException("");
		}
	}

	public static Long stringToLong(String value) {
		try {
			return value == null ? 0 : Long.valueOf(value);
		} catch (NumberFormatException e) {
			logger.error("NumericUtils-------convertInteger----Started ");
			return null;
			// throw new ApplicationException("");
		}
	}

	public static String convertLongToString(Long value) {
		try {
			return value == null ? "" : String.valueOf(value);
		} catch (NumberFormatException e) {
			logger.error("NumericUtils-------convertInteger----Started ");
			return null;
			// throw new ApplicationException("");
		}
	}

	public static Double stringToDouble(String value) {
		try {
			if (StringUtils.isNotBlank(value) && value != "") {
				return Double.valueOf(value);
			}
		} catch (NumberFormatException e) {
			logger.error("NumericUtils-------convertStringToDouble----Started ");
		}
		return null;
	}

	public static Double convertStringToDouble(String value) {
		try {
			if (StringUtils.isNotBlank(value) && value != "") {
				return Double.valueOf(value);
			}
		} catch (NumberFormatException e) {
			logger.error("NumericUtils-------convertStringToDouble----Started ");
		}
		return 0.0;
	}

	public static String convertIntegerToString(Integer value) {
		String val = null;
		if (value != null) {
			val = String.valueOf(value);
		} else {
			val = "";
		}

		return val;
	}

	public static String convertDoubleToString(Double value) {
		String val = null;
		if (value != null) {
			val = String.valueOf(value);
		} else {
			val = "";
		}

		return val;
	}

	public static String convertBooleanToString(Boolean value) {
		String val = null;
		if (value != null) {
			val = String.valueOf(value);
		} else {
			val = "";
		}

		return val;
	}

	public static Boolean convertStringToBoolean(String value) {
		Boolean val = null;
		if (StringUtils.isNotBlank(value)) {
			val = Boolean.valueOf(value);
		} else {
			val = true;
		}
		return val;
	}

	public static Long sumOfExtingAndCurrentAmount(Long newCount, Long exstingCount) {

		if (exstingCount != null) {
			return exstingCount + newCount;
		}

		return newCount;
	}

	public static List<Long> convertStringToListOfLong(String value) {
		try {
			List<Long> listofLong = Arrays.asList(value.split(",")).stream().map(obj -> Long.parseLong(obj.trim()))
					.collect(Collectors.toList());
			return listofLong;
		} catch (NumberFormatException e) {
			logger.error("NumericUtils-------convertStringToListOfLong----Started ", e);
			return null;
			// throw new ApplicationException("");
		}
	}

	public static String[] listOfStringToStringArray(List<String> al) {
		String[] val = new String[0];
		try {
			if (al != null && al.size() > 0) {
				String[] str = new String[al.size()];

				for (int i = 0; i < al.size(); i++) {
					str[i] = al.get(i);
				}
				return str;
			}
		} catch (StringIndexOutOfBoundsException e) {
			logger.error("NumericUtils-------listOfStringToStringArray----Started ");
		}
		return val;
	}
}
